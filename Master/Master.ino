#define echoPin 11
#define trigPin 10
#define rockerSwitchPin 8

#include <Wire.h>
#include <Encoder.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_LSM303_U.h>
#include <Adafruit_L3GD20_U.h>
#include <Adafruit_9DOF.h>
#include <Adafruit_MotorShield.h>
#include "Servo.h"
#include "utility/Adafruit_MS_PWMServoDriver.h"
#include "ultrasonic.h"


#define DELAY 100
// Directions
#define forward 1
#define backward -1

// I2c Addresses
#define FL 8
#define FR 9
#define RL 10
#define RR 11
#define motorShield1 0x61 // Bottom Shield
#define motorShield2 0x62 // Top Shield

// Motor Ports motorShield1 Bottom Shield

#define motorFL 3
#define motorFR 2
#define motorRL 4
#define motorRR 1

#define motorArm 4

#define wheelDiameter 4 // inches

#define FORWARD_HOME_DISTANCE 8.0 // inches

#define maxMotorSpeed 100

#define CLAW_OPEN 150
#define CLAW_CLOSED 65
#define CLAW_SERVO_PIN 10

Adafruit_MotorShield AFMS1 = Adafruit_MotorShield(motorShield1);
Adafruit_MotorShield AFMS2 = Adafruit_MotorShield(motorShield2);

Adafruit_DCMotor *mFL = AFMS2.getMotor(motorFL);
Adafruit_DCMotor *mFR = AFMS2.getMotor(motorFR);
Adafruit_DCMotor *mRL = AFMS2.getMotor(motorRL);
Adafruit_DCMotor *mRR = AFMS2.getMotor(motorRR);

Adafruit_DCMotor *mARM = AFMS1.getMotor(motorArm);

Encoder encARM(2, 3);

Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(30301);
Adafruit_LSM303_Mag_Unified   mag   = Adafruit_LSM303_Mag_Unified(30302);
Adafruit_L3GD20_Unified       gyro  = Adafruit_L3GD20_Unified(20);

Servo clawServo;

int DIRECTION = 1;
int LEFT = 2;
int RIGHT = -2;

long motorFLPosition = 0;
long motorFRPosition = 0;
long motorRLPosition = 0;
long motorRRPosition = 0;

int motorFLSetSpeed = 0;
int motorFRSetSpeed = 0;
int motorRLSetSpeed = 0;
int motorRRSetSpeed = 0;

unsigned long previousMillis = 0;

long armPOS = 0;

long buf = 0;

int encoderSelect = FL;


void setup() {
  Wire.begin();        // join i2c bus (address optional for master)
  Serial.begin(9600);  // start serial for output
  Serial.print("Starting");
  delay(500);
  // Motor Shields
  AFMS1.begin();
  delay(500);
  AFMS2.begin();
  delay(500);
  init9DOF();
  delay(500);
  clawServo.attach(CLAW_SERVO_PIN);

  // Range Sensor
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  // Rocker Switch
  readDirection();
  // Main program
  delay(500);
  program();
  moveArm(5);
  releaseMotors();
  Serial.print("I'm done =)!");
}

void loop() {
  //updateEncoders();
  delay(50);

}

void program2() {

}

void program() {
  Serial.print("Init Arm");
  moveArm(5);
  openClaw();
  moveArm(50);
  Serial.print("Move through tunnel");
  moveCorrected(1500, 1, 0); 
  homeForward(1); 
  homeHorizontal(1500, 2);
  Serial.print("Get first stack");
  moveCorrected(150, 0, 1);           // Position infront of block
  moveCorrected(110, 1, 0);           // Drive up to block
  moveArm(35);
  closeClaw();                        // Grab Block
  moveArm(60);
  moveCorrected(130, -1, 0);          // Back up
  rotate(184);                        // Turn Around
  Serial.print("Go to truck");
  moveCorrected(390, 0, -1);
  moveArm(35);                        // Lower arm to clear truck
  moveCorrected(1500, 1, 0);          // Drive to truck
  openClaw();                         // Drop block
  Serial.print("Leave truck");
  moveCorrected(1200, -1, 0);         // Back up
  rotate(184);                        // Turn around
  moveArm(50);                        // Bring up arm
  homeForward(1);
  moveCorrected(450, 0, -1);
  moveCorrected(150, -1, 0);
  homeForward(1);
  homeHorizontal(1500, 2);

  Serial.print("Go to second stack");
  moveCorrected(230, 0, 1);           // Position infront of block
  moveCorrected(110, 1, 0);           // Drive up to block
  moveArm(35);
  closeClaw();                        // Grab Block
  moveArm(60);
  moveCorrected(130, -1, 0);          // Back up
  rotate(185);                        // Turn Around
  Serial.print("Go to truck");
  moveCorrected(310, 0, -1);
  moveArm(35);                        // Lower arm to clear truck
  moveCorrected(1500, 1, 0);          // Drive to truck
  openClaw();                         // Drop block
  Serial.print("Leave truck");
  moveCorrected(1200, -1, 0);         // Back up
  rotate(185);                        // Turn around
  moveArm(50);                        // Bring up arm
  homeForward(1);
  moveCorrected(450, 0, -1);
  moveCorrected(150, -1, 0);
  homeForward(1);
  homeHorizontal(1500, 2);

  
  Serial.print("Go to third stack");
  moveCorrected(310, 0, 1);           // Position infront of block
  moveCorrected(110, 1, 0);           // Drive up to block
  moveArm(35);
  closeClaw();                        // Grab Block
  moveArm(60);
  moveCorrected(130, -1, 0);          // Back up
  rotate(184);                        // Turn Around
  Serial.print("Go to truck");
  moveCorrected(230, 0, -1);
  moveArm(35);                        // Lower arm to clear truck
  moveCorrected(1500, 1, 0);          // Drive to truck
  openClaw();                         // Drop block
  Serial.print("Leave truck");
  moveCorrected(1200, -1, 0);         // Back up
  rotate(184);                        // Turn around
  moveArm(50);                        // Bring up arm
  homeForward(1);
  moveCorrected(450, 0, -1);
  moveCorrected(150, -1, 0);
  homeForward(1);
  homeHorizontal(1500, 2);

  Serial.print("Go to Fourth stack");
  moveCorrected(390, 0, 1);           // Position infront of block
  moveCorrected(110, 1, 0);           // Drive up to block
  moveArm(35);
  closeClaw();                        // Grab Block
  moveArm(60);
  moveCorrected(130, -1, 0);          // Back up
  rotate(184);                        // Turn Around
  Serial.print("Go to truck");
  moveCorrected(150, 0, -1);
  moveArm(35);                        // Lower arm to clear truck
  moveCorrected(1500, 1, 0);          // Drive to truck
  openClaw();                         // Drop block
  Serial.print("Leave truck");
  /*
  moveCorrected(1200, -1, 0);         // Back up
  rotate(184);                        // Turn around
  moveArm(50);                        // Bring up arm
  homeForward(1);
  moveCorrected(450, 0, -1);
  moveCorrected(100, -1, 0);
  homeForward(1);
  homeHorizontal(1500, 2);
*/

}
void readDirection() {
  pinMode(rockerSwitchPin, INPUT);
  if (digitalRead(rockerSwitchPin) == HIGH) {
    DIRECTION = 1;
  } else {
    DIRECTION = -1;
  }
  RIGHT *= DIRECTION;
  LEFT *= DIRECTION;
  delay(DELAY);
}

// Homes forward and to the desired direction
void homeForward(int v) {
  resetEncoders();
  mFL->run(FORWARD);
  mFR->run(FORWARD);
  mRL->run(FORWARD);
  mRR->run(FORWARD);
  motorFRSetSpeed = maxMotorSpeed;
  motorRRSetSpeed = maxMotorSpeed;
  motorFLSetSpeed = maxMotorSpeed;
  motorRLSetSpeed = maxMotorSpeed;

  pulseTrig(); //send pulse to trig pin
  double distance = distanceInches();
  //Serial.println(distance);

  while (distance > FORWARD_HOME_DISTANCE) {
    pulseTrig();
    distance = distanceInches();
    updateEncoders();
    // Serial.println(distance);
    mFL->setSpeed(abs(motorFLSetSpeed));
    mFR->setSpeed(abs(motorFRSetSpeed));
    mRL->setSpeed(abs(motorRLSetSpeed));
    mRR->setSpeed(abs(motorRRSetSpeed));
    delay(1);
  }
  mFL->setSpeed(0);
  mFR->setSpeed(0);
  mRL->setSpeed(0);
  mRR->setSpeed(0);
  resetEncoders();
  delay(DELAY);
}


// t time in ms, dir direction Left or Right
void homeHorizontal(int t, int dir) {
  Serial.print("Home Direction: "); Serial.println(dir);
  if (dir == LEFT) {
    Serial.println("Left");
    mFL->setSpeed(90);
    mFR->setSpeed(90);
    mRL->setSpeed(90);
    mRR->setSpeed(90);
    mFL->run(BACKWARD);
    mFR->run(FORWARD);
    mRL->run(FORWARD);
    mRR->run(BACKWARD);
  }
  else {
    mFL->setSpeed(90);
    mFR->setSpeed(90);
    mRL->setSpeed(90);
    mRR->setSpeed(90);
    mFL->run(FORWARD);
    mFR->run(BACKWARD);
    mRL->run(BACKWARD);
    mRR->run(FORWARD);
  }
  delay(t);

  mFL->setSpeed(0);
  mFR->setSpeed(0);
  mRL->setSpeed(0);
  mRR->setSpeed(0);

  delay(DELAY);
}

// -1 <= h <= 1,
// -1 <= v <= 1
void moveCartesian(long distance, int h, int v) {
  resetEncoders();

  v *= DIRECTION;

  mFL->run(FORWARD);
  mFR->run(FORWARD);
  mRL->run(FORWARD);
  mRR->run(FORWARD);


  motorFRSetSpeed = (h - v) * maxMotorSpeed;
  motorRRSetSpeed = (h + v) * maxMotorSpeed;
  motorFLSetSpeed = (h + v) * maxMotorSpeed;
  motorRLSetSpeed = (h - v) * maxMotorSpeed;

  if (motorFLSetSpeed > 0) {
    mFL->run(FORWARD);
  } else {
    mFL->run(BACKWARD);
  }

  if (motorFRSetSpeed > 0) {
    mFR->run(FORWARD);
  } else {
    mFR->run(BACKWARD);
  }

  if (motorRLSetSpeed > 0) {
    mRL->run(FORWARD);
  } else {
    mRL->run(BACKWARD);
  }

  if (motorRRSetSpeed > 0) {
    mRR->run(FORWARD);
  } else {
    mRR->run(BACKWARD);
  }


  while (abs(motorFLPosition) < distance && abs(motorFRPosition) < distance && abs(motorRLPosition) < distance && abs(motorRRPosition) < distance) {
    updateEncoders();
    mFL->setSpeed(abs(motorFLSetSpeed));
    mFR->setSpeed(abs(motorFRSetSpeed));
    mRL->setSpeed(abs(motorRLSetSpeed));
    mRR->setSpeed(abs(motorRRSetSpeed));
    delay(1);
  }
  mFL->setSpeed(0);
  mFR->setSpeed(0);
  mRL->setSpeed(0);
  mRR->setSpeed(0);

  delay(500);
  resetEncoders();
  delay(DELAY);
}


void updateEncoders() {
  while (encoderSelect <= RR) {
    if (encoderSelect == FL) {
      Wire.requestFrom(FL, 4);
    }
    if (encoderSelect == FR) {
      Wire.requestFrom(FR, 4);
    }
    if (encoderSelect == RL) {
      Wire.requestFrom(RL, 4);
    }
    if (encoderSelect == RR) {
      Wire.requestFrom(RR, 4);
    }

    while (Wire.available()) {
      buf = 0;
      // cast to long to shift bits and prevent truncation
      buf = Wire.read();
      buf += (long)Wire.read() << 8;
      buf += (long)Wire.read() << 16;
      buf += (long)Wire.read() << 24;
      encoderSelect++;

      Serial.print(encoderSelect);
      Serial.print(" ");
      Serial.println(buf);

      if (encoderSelect == FL) {
        motorFLPosition = buf;
      }
      if (encoderSelect == FR) {
        motorFRPosition = buf;
      }
      if (encoderSelect == RL) {
        motorRLPosition = buf;
      }
      if (encoderSelect == RR) {
        motorRRPosition = buf;
      }

    }
    delay(1);
  }
  if (encoderSelect > RR) {
    encoderSelect = FL;
  }
}

void resetEncoders() {
  Wire.beginTransmission(FL);
  Wire.write(0xFF);              // sends one byte (arbitrary data)
  Wire.endTransmission();
  Wire.beginTransmission(FR);
  Wire.write(0xFF);              // sends one byte (arbitrary data)
  Wire.endTransmission();
  Wire.beginTransmission(RL);
  Wire.write(0xFF);              // sends one byte (arbitrary data)
  Wire.endTransmission();
  Wire.beginTransmission(RR);
  Wire.write(0xFF);              // sends one byte (arbitrary data)
  Wire.endTransmission();


  motorFLPosition = 0;
  motorFRPosition = 0;
  motorRLPosition = 0;
  motorRRPosition = 0;
  delay(500);
}

void rotate(int r) {
  //initialize IMU
  sensor_t sensor;
  accel.getSensor(&sensor);
  gyro.getSensor(&sensor);
  sensors_event_t event;
  double gz_offset, az_offset, angle = 0;
  long previousTime, currentTime, deltaT;

  //calibrate IMU
  delay(500);//to enssure there is no movement, may not be needed
  //take 100 samples and average them together to estimate the offset
  gz_offset = 0;
  for (int i = 0; i < 100; i++) {
    gyro.getEvent(&event);
    gz_offset += event.gyro.z;
  }
  gz_offset = gz_offset / 100;
  //take 100 samples and average them together to estimate the offset
  az_offset = 0;
  for (int i = 0; i < 100; i++) {
    accel.getEvent(&event);
    az_offset += event.acceleration.z;
  }
  az_offset = az_offset / 100;

  Serial.print("gz_offset: "); Serial.println(gz_offset);
  Serial.print("az_offset: "); Serial.println(az_offset);

  //set motors for motion
  mFL->run(FORWARD);
  mFR->run(BACKWARD);
  mRL->run(FORWARD);
  mRR->run(BACKWARD);
  motorFRSetSpeed = maxMotorSpeed;
  motorRRSetSpeed = maxMotorSpeed;
  motorFLSetSpeed = maxMotorSpeed;
  motorRLSetSpeed = maxMotorSpeed;
  mFL->setSpeed(abs(motorFLSetSpeed));
  mFR->setSpeed(abs(motorFRSetSpeed));
  mRL->setSpeed(abs(motorRLSetSpeed));
  mRR->setSpeed(abs(motorRRSetSpeed));
  previousTime = micros();//time from motion start

  Serial.println("motors set");

  //read and update angles until destination angle is reached
  while (angle < r) {
    //read accelerometer data
    accel.getEvent(&event);
    double az = event.acceleration.z;
    //read gyroscope data
    gyro.getEvent(&event);
    double gz = event.gyro.z;
    //measure the time between last update and now
    currentTime = micros();//time of this measurement
    deltaT = currentTime - previousTime;
    //this estimates the signifigance the accelerometer plays in measuring angle
    double a = 0.75 / (0.75 + deltaT);
    //if the change is very small don't measure
    if (!(gz - gz_offset < 0.01 && gz - gz_offset > -0.01)) {
      angle = (1 - a) * (angle + (gz - gz_offset) * deltaT / 16000.0f) + (a) * (az - az_offset);
    }
    previousTime = currentTime;//set time of last measurment
    delay(10);
  }
  //shut off motors desired angle reached
  mFR->run(RELEASE);
  mFL->run(RELEASE);
  mRR->run(RELEASE);
  mRL->run(RELEASE);

  Serial.print("Final Angle: "); Serial.println(angle);
  resetEncoders();
  delay(DELAY);
}

void releaseMotors() {
  mFR->run(RELEASE);
  mFL->run(RELEASE);
  mRR->run(RELEASE);
  mRL->run(RELEASE);

  mARM->run(RELEASE);


}

//Move to degree position
void moveArm(int degree) {
  previousMillis = millis();
  while (true) {

    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= 2000) {
      // save the last time you blinked the LED
      previousMillis = currentMillis;
      break;
    }


    Serial.println(-encARM.read());

    if (-encARM.read() > degree + 1) {
      // Move arm down
      mARM->setSpeed(50);
      mARM->run(FORWARD);
    }
    if (-encARM.read() < degree - 1) {
      // Move arm up
      mARM->setSpeed(150);
      mARM->run(BACKWARD);
    }

    if (-encARM.read() < degree + 1 && -encARM.read() > degree - 1) {
      break;
    }
  }
  mARM->setSpeed(5);

  Serial.println("Done");
  delay(DELAY);
}

void init9DOF() {
  if (!accel.begin())
  {
    /* There was a problem detecting the ADXL345 ... check your connections */
    Serial.println(F("Ooops, no LSM303 detected ... Check your wiring!"));
    while (1);
  }
  if (!mag.begin())
  {
    /* There was a problem detecting the LSM303 ... check your connections */
    Serial.println("Ooops, no LSM303 detected ... Check your wiring!");
    while (1);
  }
  if (!gyro.begin())
  {
    /* There was a problem detecting the L3GD20 ... check your connections */
    Serial.print("Ooops, no L3GD20 detected ... Check your wiring or I2C ADDR!");
    while (1);
  }
  delay(DELAY);
}

void openClaw() {
  clawServo.write(CLAW_OPEN);
  delay(1000);
}


void closeClaw() {
  clawServo.write(CLAW_CLOSED);
  delay(1000);
}

void moveCorrected(long distance, int h, int v) {
  v *= DIRECTION;
  resetEncoders();
  int motorSpeed = maxMotorSpeed;
  //initialize IMU
  sensor_t sensor;
  accel.getSensor(&sensor);
  gyro.getSensor(&sensor);
  sensors_event_t event;
  double gz_offset, az_offset, angle = 0;
  long previousTime, currentTime, deltaT;

  //calibrate IMU
  delay(500);//to enssure there is no movement, may not be needed
  //take 100 samples and average them together to estimate the offset
  gz_offset = 0;
  for (int i = 0; i < 100; i++) {
    gyro.getEvent(&event);
    gz_offset += event.gyro.z;
  }
  gz_offset = gz_offset / 100;
  //take 100 samples and average them together to estimate the offset
  az_offset = 0;
  for (int i = 0; i < 100; i++) {
    accel.getEvent(&event);
    az_offset += event.acceleration.z;
  }
  az_offset = az_offset / 100;

  motorFRSetSpeed = (h - v) * maxMotorSpeed;
  motorRRSetSpeed = (h + v) * maxMotorSpeed;
  motorFLSetSpeed = (h + v) * maxMotorSpeed;
  motorRLSetSpeed = (h - v) * maxMotorSpeed;
  if (motorFRSetSpeed > 0)
    mFR->run(FORWARD);
  else
    mFR->run(BACKWARD);
  if (motorFLSetSpeed > 0)
    mFL->run(FORWARD);
  else
    mFL->run(BACKWARD);
  if (motorRLSetSpeed > 0)
    mRL->run(FORWARD);
  else
    mRL->run(BACKWARD);
  if (motorRRSetSpeed > 0)
    mRR->run(FORWARD);
  else
    mRR->run(BACKWARD);
  mFL->setSpeed(abs(motorFLSetSpeed));
  mFR->setSpeed(abs(motorFRSetSpeed));
  mRL->setSpeed(abs(motorRLSetSpeed));
  mRR->setSpeed(abs(motorRRSetSpeed));

  while (abs(motorFLPosition) < distance
         && abs(motorFRPosition) < distance
         && abs(motorRLPosition) < distance
         && abs(motorRRPosition) < distance) {

    updateEncoders();

    //read accelerometer data
    accel.getEvent(&event);
    double az = event.acceleration.z;
    //read gyroscope data
    gyro.getEvent(&event);
    double gz = event.gyro.z;
    //measure the time between last update and now
    currentTime = micros();//time of this measurement
    deltaT = currentTime - previousTime;
    //this estimates the signifigance the accelerometer plays in measuring angle
    double a = 0.75 / (0.75 + deltaT);
    //if the change is very small don't measure
    if (!(gz - gz_offset < 0.01 && gz - gz_offset > -0.01)) {
      angle = (1 - a) * (angle + (gz - gz_offset) * deltaT / 16000.0f) + (a) * (az - az_offset);
    }
    previousTime = currentTime;//set time of last measurment


    motorFRSetSpeed = (h - v) * maxMotorSpeed + 2 * angle;
    motorRRSetSpeed = (h + v) * maxMotorSpeed + 2 * angle;
    motorFLSetSpeed = (h + v) * maxMotorSpeed - 2 * angle;
    motorRLSetSpeed = (h - v) * maxMotorSpeed - 2 * angle;
    if (motorFRSetSpeed > 0)
      mFR->run(FORWARD);
    else
      mFR->run(BACKWARD);
    if (motorFLSetSpeed > 0)
      mFL->run(FORWARD);
    else
      mFL->run(BACKWARD);
    if (motorRLSetSpeed > 0)
      mRL->run(FORWARD);
    else
      mRL->run(BACKWARD);
    if (motorRRSetSpeed > 0)
      mRR->run(FORWARD);
    else
      mRR->run(BACKWARD);
    mFL->setSpeed(abs(motorFLSetSpeed));
    mFR->setSpeed(abs(motorFRSetSpeed));
    mRL->setSpeed(abs(motorRLSetSpeed));
    mRR->setSpeed(abs(motorRRSetSpeed));

    Serial.print(angle); Serial.print(" | "); Serial.print(gz - gz_offset); Serial.print(" | "); Serial.print(az - az_offset); Serial.print(" | "); Serial.println(deltaT);
  }
  mFL->setSpeed(0);
  mFR->setSpeed(0);
  mRL->setSpeed(0);
  mRR->setSpeed(0);
  delay(DELAY);
}

