

void pulseTrig()
{
 digitalWrite(trigPin, LOW);
 delayMicroseconds(10);
 digitalWrite(trigPin, HIGH);
 delayMicroseconds(5);
 digitalWrite(trigPin, LOW);
}

double distanceInches()
{
  double duration = pulseIn(echoPin, HIGH); 
  return (duration/2.0)/74.0;
}

 double distanceCentimeters()
{
  double duration = pulseIn(echoPin, HIGH);
  return ((duration/2.0)/29.1);
}


