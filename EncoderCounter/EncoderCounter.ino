#include <Encoder.h>
#include <Wire.h>

// I2c Addresses
#define FL 8
#define FR 9
#define RL 10
#define RR 11

#define invert 1

#define address RL

//initialize the encoder using pins 2 & 3 becuase they are the only
//ones that can be used as hardware interupts.
Encoder encoder(2, 3);

int ledState = LOW;
unsigned long previousMillis = 0;        // will store last time LED was updated
#define interval 500

signed long encoderPosition = 0;

byte buf[4];

void setup() {
  pinMode(13, OUTPUT);
  Wire.begin(address);                // join i2c bus with address #8
  Wire.onRequest(requestEvent);       // register event
  Wire.onReceive(receiveEvent); // register event
}

void loop() {
  encoderPosition = encoder.read() * invert;

  buf[0] = (byte)(encoderPosition);
  buf[1] = (byte)(encoderPosition >> 8);
  buf[2] = (byte)(encoderPosition >> 16);
  buf[3] = (byte)(encoderPosition >> 24);

  //Led blink only
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;

    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
    }

    // set the LED with the ledState of the variable:
    digitalWrite(13, ledState);
  }
  delay(50);
}

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() {
  Wire.write(buf, 4);
}

// Clears encoder when activated
void receiveEvent(int howMany) {
  while (Wire.available()) {
    byte data = Wire.read(); // receive byte , unused
    encoder.write(0);
  }

}
