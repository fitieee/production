
Power Supply is in the back of the robot 

## I2C Addresses ##

```
#define FR 9
#define RL 10
#define RR 11
#define motorShield1 61 // Bottom Shield
#define motorShield2 62 // Top Shield
```

From left to right

```
Arduion mini pro 1 => FL
Arduion mini pro 2 => FR Inverted
Arduion mini pro 3 => RL
Arduion mini pro 4 => RR Inverted
```

## Motor Ports ##

```
// Motor Ports motorShield1 Bottom Shield

#define motorFL 3
#define motorFR 2
#define motorRL 4
#define motorRR 1
```